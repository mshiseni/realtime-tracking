import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SimulateProvider } from '../simulate/simulate';
import 'rxjs/add/operator/map';

/*
  Generated class for the CarProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CarProvider {

  public  simulate: SimulateProvider;

  constructor() {
    // console.log('Hello CarProvider Provider');
    this.simulate = new SimulateProvider();
  }

  pollForRiderPickup(){
    return this.simulate.riderPickedUp();
  }

  pollForRiderDropOff(){
    return this.simulate.riderDroppedOff();
  }

  dropoffCar(pickupLocation, dropoffLocation){
    return this.simulate.dropoffPickupCar(pickupLocation, dropoffLocation);
  }

  getPickupCar(){
    return this.simulate.getPickupCar();
  }

  findPickupCar(pickupLocation){
    return this.simulate.findPickupCar(pickupLocation);
  }

  getCars(lat, lng){
    return Observable
      .interval(2000)
      .switchMap(() => this.simulate.getCars(lat,lng))
      .share();
  }

}
