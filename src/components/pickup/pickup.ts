import { Component, Input, Output, EventEmitter,OnChanges, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import {InfoWindow, Marker, Map, GoogleApiWrapper} from 'google-maps-react';
import { CarProvider } from '../../providers/car/car';
import {PickupPubSubProvider } from '../../providers/pickup-pub-sub/pickup-pub-sub';
import { Observable } from 'rxjs/Observable';
import {} from '@types/googlemaps';
/**
 * Generated class for the PickupComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */

 // declare var google: any;

@Component({
  selector: 'pickup',
  templateUrl: 'pickup.html',
  providers: []
})
export class PickupComponent implements OnInit, OnChanges {
  @Input() isPinSet: boolean;
  @Input() map: google.maps.Map;
  @Input() isPickUpRequested: boolean;
  @Input() destination: string;
  @Output() updatedPickupLocation: EventEmitter<google.maps.LatLng> = new EventEmitter();

  private pickUpMarker: google.maps.Marker;
  private popup: google.maps.InfoWindow;
  private pickupSubscription: any;
  // text: string;

  constructor(private pickupPubSub: PickupPubSubProvider) {
    // console.log('Hello PickupComponent Component');
    // this.text = 'Hello World';
  }

  ngOnInit(){
    this.pickupSubscription = this.pickupPubSub.watch().subscribe( e => {
      if(e.event === this.pickupPubSub.EVENTS.ARRIVAL_TIME){
        this.updateTime(e.data);
      }
    })
  }

  ngOnChanges(changes){
    //do not allow pickup pin/location
    //to change if pickup is requested
  if(!this.isPickUpRequested){
      if(this.isPinSet){
        console.log('SHOW pickup marker')
        this.showPickUpMarker();
      }else{
        console.log('REMOVE pickup marker')
        this.removePickUpMarker();
      }
    }

    if(this.destination){
      this.removePickUpMarker();
    }
  }

  showPickUpMarker(){

    this.removePickUpMarker();

    this.pickUpMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.BOUNCE,
      position: this.map.getCenter(),
      icon: 'img/mn.png'
    })

    this.showPickUpTime();

    //send pickup location
    this.updatedPickupLocation.next(this.pickUpMarker.getPosition());
  }

  removePickUpMarker(){
    if(this.pickUpMarker){
      this.pickUpMarker.setMap(null);
      this.pickUpMarker = null;
    }
  }

  showPickUpTime(){
    this.popup = new google.maps.InfoWindow({
      content: '<h5> Yo are here </h5>'
    });

    this.popup.open(this.map,this.pickUpMarker);

    google.maps.event.addListener(this.pickUpMarker, 'click', () => {
      this.popup.open(this.map, this.pickUpMarker);
    })
  }

  updateTime(seconds){
    let minutes = Math.floor(seconds/60);
    this.popup.setContent(`<h5>${minutes} minutes</h5>`);
  }
}
