import { Component } from '@angular/core';
import { AlertController,NavController } from 'ionic-angular';
import {PickupPubSubProvider } from '../../providers/pickup-pub-sub/pickup-pub-sub';
import { DestinationAddressComponent } from '../../components/destination-address/destination-address';
// import { MapComponent } from '../../components/map/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PickupPubSubProvider]
  // directives: [MapComponent]
})
export class HomePage {

  public isPickUpRequested: boolean;
  public isRiderPickedUp: boolean;
  public pickupSubscription: any;
  public timeTillArrival: number;
  public destination: string;

  constructor(public alert: AlertController, private pickupPubSub: PickupPubSubProvider) {
    this.isPickUpRequested = false;
    this.isRiderPickedUp = false;
    this.timeTillArrival = 5;
    this.pickupSubscription = this.pickupPubSub.watch().subscribe(e => {
      this.processPickupSubscription(e);
    })
  }

  processPickupSubscription(e){
    switch(e.event){
      case this.pickupPubSub.EVENTS.ARRIVAL_TIME:
        this.updateArrivalTime(e.data);
        break;
      case this.pickupPubSub.EVENTS.PICKUP:
        this.riderPickedUp();
        break;
    }
  }

  setDestination(destination){
    this.destination = destination;
  }

  riderPickedUp(){
    this.isRiderPickedUp = true;
    this.rateDriver();
  }

  rateDriver(){
    let prompt = this.alert.create({
      title: 'Rate Driver',
      message: 'Select a rating for the Driver',
      inputs: [{
          type: 'radio',
          label: 'Perfect',
          value: 'perfect',
          checked: true
        },
        {
            type: 'radio',
            label: 'Okay',
            value: 'okay'
          },
          {
              type: 'radio',
              label: 'Bad',
              value: 'bad'
          }],
          buttons: [{
            text: 'Submit',
            handler: rating => {
              //TODO: send ratings to server
              console.log(rating);
            }
          }]
    })

    prompt.present();
  }

  riderDroppedOff(){
    this.rateDriver();
    this.isRiderPickedUp = false;
    this.isPickUpRequested = false;
    this.destination = null;
    this.timeTillArrival = 5;
  }

  updateArrivalTime(seconds){
    let minutes = Math.floor(seconds/60);
    this.timeTillArrival = minutes;
  }

  confirmPickUp(){
    this.isPickUpRequested = true;
  }

  cancelPickUp(){
    this.isPickUpRequested = false;
  }

}
