import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CarProvider } from '../../providers/car/car';
// import SlidingMarker = require('marker-animate-unobtrusive');
import * as SlidingMarker  from "../../../node_modules/marker-animate-unobtrusive";
import {PickupPubSubProvider } from '../../providers/pickup-pub-sub/pickup-pub-sub';


/**
 * Generated class for the PickupCarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'pickup-car',
  templateUrl: 'pickup-car.html'
})
export class PickupCarComponent implements OnInit, OnChanges{

  @Input() map: google.maps.Map;
  @Input() isPickUpRequested: boolean;
  @Input() pickupLocation: google.maps.LatLng;
  @Input() destination: string;

  public pickupCarMarker: any;
  public polylinePath: google.maps.Polyline;

  constructor(public carService: CarProvider, private pickupPubSub: PickupPubSubProvider) {
  }

  ngOnInit(){

  }

  ngOnChanges(){

    if(this.destination){
      this.dropoffCar();
    }else{
      if(this.isPickUpRequested){
        //request a Car
        this.requestCar();
      }else{
        //remove a Car
        this.removeCar();
        this.removeDirections();
      }
    }
  }

  addCarMarker(position){
    this.pickupCarMarker = new SlidingMarker({
      map: this.map,
      position: position,
      icon: 'img/c.png'
    });

    this.pickupCarMarker.setDuration(1000);
    this.pickupCarMarker.setEasing('linear');
  }

  showDirections(path){
    this.polylinePath = new google.maps.Polyline({
      path: path,
      strokeColor: '#FF0000',
      strokeWeight: 3
    });
    this.polylinePath.setMap(this.map);
  }

  updateCar(cbDone){
    this.carService.getPickupCar().subscribe(car => {
      console.log("========CAR============");
      console.log(car);
      //animate care to next position
      this.pickupCarMarker.setPosition(car.position);
      //set direction path for car
      this.polylinePath.setPath(car.path);
      //update arrival time
      this.pickupPubSub.emitArrivalTime(car.time);
      //keep updating car
      if(car.path.length > 1){
        setTimeout( () => {
          this.updateCar(cbDone);
        },1000);
      }else{
        //car arrived
        cbDone();
      }
    });
  }

  checkForRiderPickup(){
    this.carService.pollForRiderPickup().subscribe( data => {
      this.pickupPubSub.emitPickUp();
    })
  }

  checkForRiderDropoff(){
    this.carService.pollForRiderDropOff().subscribe( data => {
      this.pickupPubSub.emitDropOff();
    })
  }

  requestCar(){
    console.log('request a car ' + this.pickupLocation);
    this.carService.findPickupCar(this.pickupLocation)
      .subscribe(car => {
        //show car Marker
        this.addCarMarker(car.position);
        //show car direction to you
        this.showDirections(car.path);
        //keep updating car
        this.updateCar(() => this.checkForRiderPickup() );
      })
  }

  dropoffCar(){
    this.carService.dropoffCar(this.pickupLocation, this.destination)
    .subscribe( car => {
      //keep updating car
      this.updateCar(() => this.checkForRiderDropoff() );
    })
  }

  removeDirections(){
    if(this.polylinePath){
      this.polylinePath.setMap(null);
      this.polylinePath = null;
    }
  }

  removeCar(){
    if(this.pickupCarMarker){
      this.pickupCarMarker.setMap(null);
      this.pickupCarMarker = null;
    }
  }
}
