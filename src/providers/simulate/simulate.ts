import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class SimulateProvider {

  public directionsService: google.maps.DirectionsService;
  public myRoute: any;
  public myRouteIndex: number;

  constructor() {
    // console.log('Hello SimulateProvider Provider');
    this.directionsService = new google.maps.DirectionsService();
  }

  riderPickedUp(){
    //simulate rider picked up after 1 second
    return Observable.timer(1000);
  }

  riderDroppedOff(){
    //simulate rider dropped off after 1 second
    return Observable.timer(1000);
  }

  getPickupCar(){
    return Observable.create(observable => {
      let car = this.myRoute[this.myRouteIndex];
      observable.next(car);
      this.myRouteIndex++;
    })
  }

  getSegmentedDirections(directions){
    let route = directions.routes[0];
    let legs = route.legs;
    let path = [];
    let increment = [];
    let duration = 0;

    let numOfLegs = legs.length;

    // works backward  through each leg in directions route
    while(numOfLegs--){
      let leg = legs[numOfLegs];
      let steps = leg.steps;
      let numOfSteps = steps.length;

      while(numOfSteps--){
        let step = steps[numOfSteps];
        let points = step.path;
        let numOfPoints = points.length;

        duration += step.duration.value;

        while(numOfPoints--){
          let point = points[numOfPoints];
          path.push(point);
          increment.unshift({
            position: point, //car position
            time: duration, //time left for arrival
            path: path.slice(0) //clone array to prevent referncing final path array
          })
        }
      }
    }

    return increment;
  }
  calculateRoute(start, end){
    return Observable.create(observable => {
      this.directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
      },(response, status) => {
        if(status === google.maps.DirectionsStatus.OK){
          observable.next(response);
        }else{
          observable.error(status);
        }
      })
    });
  }

  simulateRoute(start, end){
    return Observable.create(observable => {
      this.calculateRoute(start, end).subscribe(directions => {
        //get route path
        this.myRoute = this.getSegmentedDirections(directions);
        //return pickup car
        this.getPickupCar().subscribe(car => {
          observable.next(car);
        })
      })
    });
  }

  findPickupCar(pickupLocation){

    this.myRouteIndex = 0;

    let car = this.cars1.cars[0];//pick one car to simulate pickupLocation
    let start = new google.maps.LatLng(car.coord.lat, car.coord.lng);
    let end = pickupLocation;

    return this.simulateRoute(start, end);
  }

  dropoffPickupCar(pickupLocation, dropoffPickupCar){
    return this.simulateRoute(pickupLocation, dropoffPickupCar);
  }

  getCars(lat, lng){
    let carData = this.cars[this.carIndex];

    this.carIndex++;

    if(this.carIndex > this.cars.length-1){
      this.carIndex = 0;
    }

    return Observable.create(
      observer => observer.next(carData)
    )
  }
  private carIndex: number = 0;

  private cars1 = {
    cars: [{
      id: 1,
      coord: {
        lat: -26.082154,
        lng: 28.085078
      }
    },
    {
      id: 2,
      coord: {
        lat: -26.085025,
        lng: 28.086129
      }
    }]
  };

  private cars2 = {
    cars: [{
      id: 1,
      coord: {
        lat: -26.082126,
        lng: 28.086626
      }
    },
    {
      id: 2,
      coord: {
        lat: -26.084948,
        lng: 28.082975
      }
    }]
  };

  private cars3 = {
    cars: [{
      id: 1,
      coord: {
        lat: -26.083735,
        lng: 28.086368
      }
    },
    {
      id: 2,
      coord: {
        lat: -26.086239,
        lng: 28.082954
      }
    }]
  };

  private cars4 = {
    cars: [{
      id: 1,
      coord: {
        lat: -26.083643,
        lng: 28.085086
      }
    },
    {
      id: 2,
      coord: {
        lat: -26.086215,
        lng: 28.085968
      }
    }]
  };

  private cars: Array<any> = [this.cars1, this.cars2, this.cars3, this.cars4];
}
