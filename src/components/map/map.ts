import { Component, OnInit, Input } from '@angular/core';
import { Loading, NavController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';
import { PickupComponent } from '../../components/pickup/pickup';
import { AvailableCarsComponent } from '../available-cars/available-cars';
import { CarProvider } from '../../providers/car/car';
import { PickupCarComponent } from '../pickup-car/pickup-car';

/**
 * Generated class for the MapComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */

@Component({
  selector: 'map',
  templateUrl: 'map.html',
  providers: []
})
export class MapComponent implements OnInit{

  @Input() isPickUpRequested: boolean;
  @Input() destination: string;

  // text: string;
  public map: google.maps.Map;
  public isMapIdle: boolean;
  public currentLocation: google.maps.LatLng;

  constructor(public nav: NavController,public geolocation: Geolocation,public loadingCtrl: LoadingController) {
    // console.log('Hello MapComponent Component');
    // this.text = 'Hello World';

  }

  ngOnInit(){
    this.map = this.createMap();
    this.addMapEventListener();

    this.getCurrentPosition().subscribe(location => {
      // this.map.panTo(location);
      this.centerLocation(location);
    });
  }

  updatePickupLocation(location){
    this.currentLocation = location;
    this.centerLocation(location);
  }

  addMapEventListener(){
    google.maps.event.addListener(this.map, 'dragstart', () => {
      this.isMapIdle = false;
    })
    google.maps.event.addListener(this.map, 'idle', () => {
      this.isMapIdle = true;
    })
  }

  getCurrentPosition(){

    let loading = this.loadingCtrl.create({
      content: 'Locating....'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 5000);

    // this.nav.pop();

    let options = {timeout: 10000, enableHighAccuracy: true};

    let locationObs = Observable.create(observable => {
      this.geolocation.getCurrentPosition(options)
      .then(resp => {
          let lat = resp.coords.latitude;
          let lng = resp.coords.longitude;
          console.log("===================");
          console.log(lat);
          console.log(lng);
          let location = new google.maps.LatLng(lat, lng);

          observable.next(location);

          loading.dismiss();
        },
        (err) => {
          console.log('Geolocation err: ' + err);
          loading.dismiss();
        })
    })
    return locationObs;
  }

  createMap(location = new google.maps.LatLng(26.1076,28.0567)){
    let mapOptions = {
      center: location,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }

    let mapEl = document.getElementById('map');
    let map = new google.maps.Map(mapEl, mapOptions);

    return map;
  }

  centerLocation(location){
    if(location){
      this.map.panTo(location);
    }else{
      this.getCurrentPosition().subscribe(currentLocation => {
        this.map.panTo(currentLocation);
      })
    }
  }

}
