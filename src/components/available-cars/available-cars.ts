import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CarProvider } from '../../providers/car/car';
import * as SlidingMarker  from "../../../node_modules/marker-animate-unobtrusive";
// import SlidingMarker = require('marker-animate-unobtrusive');

/**
 * Generated class for the AvailableCarsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'available-cars',
  templateUrl: 'available-cars.html',
  providers:[]
})
export class AvailableCarsComponent implements OnInit{

  @Input() map: google.maps.Map;
  @Input() isPickUpRequested: boolean;

  public carMarkers: Array<google.maps.Marker>;

  constructor(public carService: CarProvider) {
    // console.log('Hello AvailableCarsComponent Component');
    // this.text = 'Hello World';
    this.carMarkers = [];
  }

  ngOnInit(){
    this.fetchAndRefreshCars();
  }

  ngOnChanges(){
    if(this.isPickUpRequested){
      this.removeCarMarkers();
    }
  }

  removeCarMarkers(){
    let numOfCars = this.carMarkers.length;
    while(numOfCars--){
      let car = this.carMarkers.pop();
      car.setMap(null);
    }
  }

  addcarMarker(car){
    let carMarker = new SlidingMarker({
      map: this.map,
      position: new google.maps.LatLng(car.coord.lat, car.coord.lng),
      icon: 'img/c.png',
      draggable: true
    });

    carMarker.setDuration(2000);
    carMarker.setEasing('linear');

    carMarker.set('id',car.id); //MVC Object

    this.carMarkers.push(carMarker);
  }


  updateCarMarker(car){
    for (var i = 0,numOfCars = this.carMarkers.length; i < numOfCars; i++){
      // find car and update it
      if(this.carMarkers[i].id === car.id){
          this.carMarkers[i].setPosition(new google.maps.LatLng(car.coord.lat,car.coord.lng));
          return
      }
      // console.log(this.carMarkers[0]);
    }

    //car does not exist in carmarker
    this.addcarMarker(car);
  }

  fetchAndRefreshCars(){
    this.carService.getCars(9,9)
    .subscribe(carData => {

      if(!this.isPickUpRequested){
        (<any>carData).cars.forEach( car => {
          this.updateCarMarker(car);
        })
      }
    })
  }

}
